# Sudoku

[![Hackage](https://img.shields.io/hackage/v/Sudoku.svg?logo=haskell)](https://hackage.haskell.org/package/Sudoku)
[![MIT license](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)
[![Stackage Lts](http://stackage.org/package/Sudoku/badge/lts)](http://stackage.org/lts/package/Sudoku)
[![Stackage Nightly](http://stackage.org/package/Sudoku/badge/nightly)](http://stackage.org/nightly/package/Sudoku)
[![Build status](https://img.shields.io/travis/Reyu/Sudoku.svg?logo=travis)](https://travis-ci.org/Reyu/Sudoku)

See README for more info
